(defsystem     "clap"
  :version     "0.0.1"
  :serial      t
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :maintainer  "contrapunctus <contrapunctus at disroot dot org>"
  :description "Inter-application data exchange for CLIM"
  :depends-on  ("mcclim" "clim-app-base" "closer-mop" "alexandria")
  :components  ((:module "src"
                 :serial t
                 :components ((:file "package")
                              (:file "core")
                              (:file "intents")
                              (:file "emacsclient")))))
