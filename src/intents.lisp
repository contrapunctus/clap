(in-package :clap.intents)

(clap:define-intent view)
(clap:define-intent edit)
(clap:define-intent share)
(clap:define-intent append)  ;; "enqueue"?
(clap:define-intent search)
(clap:define-intent archive)
(clap:define-intent extract)
(clap:define-intent compress)
(clap:define-intent decompress)
