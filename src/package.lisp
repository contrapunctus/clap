(in-package :cl)

(defpackage :clap
  (:use :cl)
  (:local-nicknames (:cab :clim-app-base)
                    (:u :uiop)
                    (:cm :closer-mop)
                    (:a :alexandria))
  ;; sending
  (:export #:define-intent
           #:receivers
           #:com-return)
  ;; receiving
  (:export #:receive
           #:com-insert-from-application
           #:senders))

(defpackage :clap.intents)
