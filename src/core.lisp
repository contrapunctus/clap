(in-package :clap)

(defclass application ()
  ((%name :initarg :name
          :accessor name)
   (%description :initarg :description
                 :accessor description)))

;; application selection GUI


;;;; SENDING
(defmacro define-intent (name type)
  "Define a global command com-NAME and a generic function NAME.

NAME should be a verb, e.g. VIEW, EDIT, SHARE, etc."
  (let ((abbrev    (intern (concatenate 'string name "able")))
        (type-list (intern (concatenate 'string "*" name "-types" "*"))))
    `(progn
       ;; define an extensible type
       (defvar ,type-list '())
       (clim:define-presentation-type-abbreviation ,abbrev ()
         (or ,@type-list))
       (defgeneric ,name (object application &key &allow-other-keys)
         (:documentation ,(format nil "~A OBJECT in APPLICATION." name)))
       (clim:define-command ,(intern (concatenate 'string "com-" (symbol-name name)))
           ((object ,type))
         ;; TODO - show a menu of applications which can handle this intent and object
         (let* ((receivers (receivers ',name ',type))
                (first     (first receivers)))
           ;; then...
           (cond
             ;; singleton list
             ((and first (null (rest receivers)))
              (,name object first))
             )))
       ;; make com-NAME appear in context menus (e.g. via presentation action)
       )))

(defmacro register
    (application intent type
     (object-var application-var &rest keys &key &allow-other-keys)
     &body body)
  "Register APPLICATION as handler for INTENT on objects of TYPE.

APPLICATION should be an instance of `clap:application'.

INTENT and TYPE should be names of existing intents (as defined by
`clap:define-intent') and types.

The fourth argument is used to define a specialized lambda list for
BODY. The symbols OBJECT-VAR and APPLICATION-VAR are bound within BODY
to TYPE and the instance of `clap:application', respectively.

BODY is executed when APPLICATION is selected to handle an object of
TYPE for INTENT."
  (let ((type-list (intern (concatenate 'string "*" name "-types" "*"))))
    `(progn
       (pushnew ,type ,type-list)
       (when (symbol-function ',intent)
         (defmethod ,intent
             ((,object-var ,type)
              (,application-var (class-name (class-of ,application)))
              &key &allow-other-keys ,@keys)
           ,@body)))))

;; the correct term is probably 'recipients', but eh...
(defun receivers (intent class)
  "List of `application' instances which can handle INTENT on an instance of CLASS.
INTENT and CLASS should be symbols."
  (loop
    :for (m-class m-app)
      :in (mapcar #'cm:method-specializers
                  (cm:generic-function-methods (symbol-function intent)))
    :when (cm:subclassp m-class class)
      :collect m-app))

(clim:define-command com-return ((object t)))

;;;; RECEIVING
;; REVIEW - it may be possible to implement 'receiving' by using `clim:accept'
(defgeneric receive (object application &key &allow-other-keys)
  (:documentation "Receive OBJECT in APPLICATION."))

(clim:define-command com-insert-from-application ((type t)))

(defun senders (type)
  "List of applications which can return an object of TYPE.")
