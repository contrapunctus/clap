(in-package :clap)

(defclass emacsclient (application) ())

(defmethod view ((file cab:file) (emacsclient emacsclient))
  (u:launch-program "emacsclient" :input (cab:absolute-pathname file)))
